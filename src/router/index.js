import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Logout from '../views/Logout.vue'
import SignIn from '../views/SignIn.vue'
import Configuration from '../views/Configuration.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/sign-in',
    name: 'SignIn',
    component: SignIn
  },
  {
    path: '/logout',
    name: 'Logout',
    component: Logout,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/configuration',
    name: 'Configuration',
    component: Configuration,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/system',
    name: 'System',
    component: function () {
      return import('../views/System.vue')
    },
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/categories',
    name: 'Categories',
    component: function () {
      return import('../views/Categories.vue')
    }
  },
  {
    path: '/products',
    component: function () {
      return import('../views/products/Products.vue')
    },
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '',
        name: 'index-product',
        component: function () {
          return import('../views/products/Index.vue')
        }
      },
      {
        path: 'new',
        name: 'new-product',
        component: function () {
          return import('../views/products/New.vue')
        }
      }
    ]
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: function () {
      return import(/* webpackChunkName: "dashboard" */ '../views/Dashboard.vue')
    },
    meta: {
      requiresAuth: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  linkActiveClass: 'is-active',
  routes
})

export default router
