import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    titleApp: process.env.VUE_APP_TITLE,
    token: localStorage.getItem('access_token') || null,
    alert: {
      title: process.env.VUE_APP_TITLE,
      color: '',
      text: '',
      status: false
    }
  },
  getters: {
    loggedIn (state) {
      return state.token !== null
    },
    getToken (state) {
      return state.token
    }
  },
  mutations: {
    retrieveToken (state, token) {
      state.token = token
    },
    destroyToken (state) {
      state.token = null
    },
    setAlert (state, payload) {
      state.alert.color = payload.color
      state.alert.text = payload.text
      state.alert.status = payload.status
    }
  },
  actions: {
    setAlert (context, payload) {
      context.commit('setAlert', payload)
    },
    retrieveToken (context, credentials) {
      return new Promise((resolve, reject) => {
        Vue.axios.post('/login', {
          username: credentials.username,
          password: credentials.password
        })
          .then(response => {
            // console.log(response)
            const token = response.data.access_token
            localStorage.setItem('access_token', token)
            context.commit('retrieveToken', token)
            resolve(response)
          })
          .catch(error => {
            // console.log(error)
            reject(error)
          })
      })
    },
    destroyToken (context) {
      if (context.getters.loggedIn) {
        return new Promise((resolve, reject) => {
          Vue.axios.post('/logout', '', {
            headers: { Authorization: 'Bearer ' + context.state.token }
          })
            .then(response => {
              // console.log(response)
              localStorage.removeItem('access_token')
              context.commit('destroyToken')
              resolve(response)
            })
            .catch(error => {
              // console.log(error)
              localStorage.removeItem('access_token')
              context.commit('destroyToken')
              reject(error)
            })
        })
      }
    }
  },
  modules: {
  }
})
